# reddit-android #

Sample application that lists Reddit's Android community latest posts. The application was entirely written in Kotlin (including its tests). Slightly based on [this Clean Architecture + MVP proposal](https://github.com/android10/Android-CleanArchitecture).

### Which libraries were used? ###
* Retrofit 2;
* RxJava;
* GSON;
* Glide;