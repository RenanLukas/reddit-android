package com.lukas.renan.redditandroid.domain.base

import rx.Observable
import rx.Scheduler
import rx.Subscriber
import rx.Subscription
import rx.subscriptions.Subscriptions

abstract class Interactor<T>(val subscribeScheduler: Scheduler, val observeScheduler: Scheduler) {

    var subscription: Subscription = Subscriptions.empty()

    abstract fun buildObservable(): Observable<T>

    fun execute(subscriber: Subscriber<T>) {
        subscription = buildObservable()
                .subscribeOn(subscribeScheduler)
                .observeOn(observeScheduler)
                .subscribe(subscriber)
    }

    fun unsubscribe() = subscription.unsubscribe()
}