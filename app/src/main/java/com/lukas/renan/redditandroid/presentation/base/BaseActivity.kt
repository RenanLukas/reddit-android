package com.lukas.renan.redditandroid.presentation.base

import android.os.Build
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.lukas.renan.redditandroid.R

abstract class BaseActivity<Presenter : BaseContract.Presenter> : AppCompatActivity(), BaseContract.View<Presenter> {

    protected var presenter: Presenter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        configureToolbar()
        presenter = createPresenter()
    }

    override fun onPause() {
        super.onPause()
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.M) {
            presenter?.stop()
        }
    }

    override fun onStop() {
        super.onStop()
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.N) {
            presenter?.stop()
        }
    }

    override fun onResume() {
        super.onResume()
        presenter?.resume()
    }

    private fun configureToolbar() {
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setIcon(R.mipmap.ic_launcher)
    }
}
