package com.lukas.renan.redditandroid.presentation.base

interface BaseContract {

    interface View<out Presenter : BaseContract.Presenter> {

        fun createPresenter(): Presenter

    }

    interface Presenter {
        fun resume()

        fun stop()

        fun finish()
    }
}