package com.lukas.renan.redditandroid.data.postlist.entity

data class RedditPostData(val kind: String, val data: RedditPostDetails)
