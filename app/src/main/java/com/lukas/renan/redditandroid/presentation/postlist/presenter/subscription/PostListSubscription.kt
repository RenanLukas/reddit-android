package com.lukas.renan.redditandroid.presentation.postlist.presenter.subscription

import com.lukas.renan.redditandroid.domain.base.DomainSubscription
import com.lukas.renan.redditandroid.domain.postlist.Post
import com.lukas.renan.redditandroid.presentation.postlist.PostListContract

class PostListSubscription(val view: PostListContract.View) : DomainSubscription<List<Post>>() {

    override fun onCompleted() {
        super.onCompleted()
        view.showListLayout()
    }

    override fun onError(e: Throwable?) {
        super.onError(e)
        view.showErrorLayout()
    }

    override fun onNext(posts: List<Post>) {
        super.onNext(posts)
        view.updatePostList(posts)
    }
}