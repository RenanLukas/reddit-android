package com.lukas.renan.redditandroid.domain.postlist

data class Post(val name: String, val title: String, val url: String, val thumbnail: String, val num_comments: Int,
                val created: Long, val openBrowser: Boolean)
