package com.lukas.renan.redditandroid.presentation.postlist.view

import android.content.res.Resources
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.lukas.renan.redditandroid.R
import com.lukas.renan.redditandroid.domain.postlist.Post
import com.lukas.renan.redditandroid.presentation.mechanism.extension.inflate
import com.lukas.renan.redditandroid.presentation.mechanism.extension.loadUrl
import rx.Observable
import rx.subjects.PublishSubject

class PostListAdapter(val resources: Resources) : RecyclerView.Adapter<PostListAdapter.ViewHolder>() {

    private val posts = mutableListOf<Post>()
    private val onClickSubject: PublishSubject<Post> = PublishSubject.create()

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        holder?.itemView?.setOnClickListener({
            onClickSubject.onNext(posts[position])
        })
        holder?.bind(posts[position])
    }

    override fun getItemCount(): Int = posts.size

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        return ViewHolder(parent?.inflate(R.layout.row_postlist, false), resources)
    }

    class ViewHolder(rootView: View?, val resources: Resources) : RecyclerView.ViewHolder(rootView) {

        val title = rootView?.findViewById(R.id.post_title) as TextView
        val image = rootView?.findViewById(R.id.post_image) as ImageView
        val commentsCount = rootView?.findViewById(R.id.comments_count) as TextView

        fun bind(post: Post) {
            title.text = post.title
            val commentsText = resources.getQuantityText(R.plurals.comments, post.num_comments)
            commentsCount.text = "${post.num_comments} $commentsText"
            image.loadUrl(post.thumbnail)
        }
    }

    fun clickObservable(): Observable<Post> = onClickSubject.asObservable()

    fun newestPostId(): String = if (posts.isNotEmpty()) posts[0].name else ""

    fun oldestPostId(): String = if (posts.isNotEmpty()) posts[posts.lastIndex].name else ""

    fun update(newPosts: List<Post>) {
        if (newPosts.isNotEmpty()) {
            posts.addAll(newPosts)
            this.notifyDataSetChanged()
        }
    }
}