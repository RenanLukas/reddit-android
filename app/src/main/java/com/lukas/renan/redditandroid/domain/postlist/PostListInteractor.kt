package com.lukas.renan.redditandroid.domain.postlist

import com.lukas.renan.redditandroid.data.postlist.repository.PostListDataRepository
import com.lukas.renan.redditandroid.domain.base.Interactor
import rx.Observable
import rx.Scheduler

open class PostListInteractor(val repository: PostListDataRepository, postListSubscribeScheduler: Scheduler,
                         postListObserveScheduler: Scheduler) : Interactor<List<Post>>(postListSubscribeScheduler, postListObserveScheduler) {

    var beforePostId: String? = null
    var afterPostId: String? = null

    override fun buildObservable(): Observable<List<Post>> = repository.fetchPostList(beforePostId, afterPostId)

    fun fetchNewerThan(beforePostId: String?) {
        this.afterPostId = null
        this.beforePostId = beforePostId
    }

    fun fetchOlderThan(afterPostId: String?) {
        this.beforePostId = null
        this.afterPostId = afterPostId
    }
}