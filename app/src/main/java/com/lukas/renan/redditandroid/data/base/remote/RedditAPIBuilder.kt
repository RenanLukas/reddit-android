package com.lukas.renan.redditandroid.data.base.remote

import com.lukas.renan.redditandroid.BuildConfig

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class RedditAPIBuilder {

    fun build(): RedditAPI {
        val okHttpBuilder = OkHttpClient().newBuilder()
        val retrofitBuilder = Retrofit.Builder()
        retrofitBuilder.baseUrl(BuildConfig.ENDPOINT)
        retrofitBuilder.client(okHttpBuilder.build())
        retrofitBuilder.addCallAdapterFactory(RxJavaCallAdapterFactory.create())
        retrofitBuilder.addConverterFactory(GsonConverterFactory.create())
        return retrofitBuilder.build().create<RedditAPI>(RedditAPI::class.java)
    }
}