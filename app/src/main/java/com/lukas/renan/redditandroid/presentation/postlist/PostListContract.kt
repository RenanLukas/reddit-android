package com.lukas.renan.redditandroid.presentation.postlist

import com.lukas.renan.redditandroid.domain.postlist.Post
import com.lukas.renan.redditandroid.presentation.base.BaseContract

interface PostListContract {

    interface View {
        fun showLoadingLayout()

        fun showErrorLayout()

        fun showListLayout()

        fun updatePostList(posts: List<Post>)

        fun navigateToExternalWebScreen(url: String)
    }

    interface Presenter : BaseContract.Presenter {
        fun onPostListPulled(newestPostId: String?)

        fun onPostClicked(post: Post)
    }
}
