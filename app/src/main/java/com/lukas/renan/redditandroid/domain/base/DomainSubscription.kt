package com.lukas.renan.redditandroid.domain.base

import rx.Subscriber

open class DomainSubscription<T> : Subscriber<T>() {
    override fun onCompleted() {
    }

    override fun onNext(t: T) {
    }

    override fun onError(e: Throwable?) {
    }
}