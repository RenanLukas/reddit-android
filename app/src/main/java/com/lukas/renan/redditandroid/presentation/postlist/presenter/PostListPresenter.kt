package com.lukas.renan.redditandroid.presentation.postlist.presenter

import com.lukas.renan.redditandroid.domain.postlist.Post
import com.lukas.renan.redditandroid.domain.postlist.PostListInteractor
import com.lukas.renan.redditandroid.presentation.postlist.PostListContract
import com.lukas.renan.redditandroid.presentation.postlist.presenter.subscription.PostListSubscription

class PostListPresenter(val view: PostListContract.View, val interactor: PostListInteractor) : PostListContract.Presenter {

    fun present() {
        view.showLoadingLayout()
        interactor.execute(PostListSubscription(view))
    }

    override fun resume() {
    }

    override fun stop() {
    }

    override fun finish() {
        interactor.unsubscribe()
    }

    override fun onPostListPulled(newestPostId: String?) {
        interactor.fetchNewerThan(newestPostId)
        interactor.execute(PostListSubscription(view))
    }

    override fun onPostClicked(post: Post) {
//        if (post.openBrowser) {
        view.navigateToExternalWebScreen(post.url)
//        } else {
        //TODO load post details screen
//        }
    }

    fun onPostListScrolledToEnd(oldestPostId: String?) {
        interactor.fetchOlderThan(oldestPostId)
        interactor.execute(PostListSubscription(view))
    }
}
