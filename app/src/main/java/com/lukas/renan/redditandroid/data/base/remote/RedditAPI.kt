package com.lukas.renan.redditandroid.data.base.remote

import com.lukas.renan.redditandroid.data.postlist.entity.RedditPostListing
import retrofit2.http.GET
import retrofit2.http.Query
import rx.Observable

interface RedditAPI {

    @GET("Android/new/.json")
    fun fetchNewAndroidPosts(@Query("limit") limit: Int, @Query("before") before: String?, @Query("after") after: String?): Observable<RedditPostListing>
}