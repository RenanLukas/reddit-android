package com.lukas.renan.redditandroid.domain.postlist

import rx.Observable

interface PostListRepository {
    fun fetchPostList(before: String?, after: String?): Observable<List<Post>>
}