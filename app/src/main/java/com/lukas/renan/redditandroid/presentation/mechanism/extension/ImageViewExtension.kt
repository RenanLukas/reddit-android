package com.lukas.renan.redditandroid.presentation.mechanism.extension

import android.view.View
import android.webkit.URLUtil
import android.widget.ImageView
import com.bumptech.glide.Glide

fun ImageView.loadUrl(url: String?) {
    if (url.isNullOrEmpty() || !URLUtil.isValidUrl(url)) {
        this.visibility = View.GONE
    } else {
        this.visibility = View.VISIBLE
        Glide.with(context).load(url).into(this)
    }
}