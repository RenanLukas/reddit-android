package com.lukas.renan.redditandroid.data.postlist.repository

import com.lukas.renan.redditandroid.data.base.remote.RedditAPI
import com.lukas.renan.redditandroid.data.postlist.mapper.RedditPostDataEntityMapper
import com.lukas.renan.redditandroid.domain.postlist.Post
import com.lukas.renan.redditandroid.domain.postlist.PostListRepository
import rx.Observable

open class PostListDataRepository(private val api: RedditAPI, private val mapper: RedditPostDataEntityMapper) : PostListRepository {

    private val REQUEST_LIMIT = 10

    override fun fetchPostList(before: String?, after: String?): Observable<List<Post>> {
        return api.fetchNewAndroidPosts(REQUEST_LIMIT, before, after)
                .map { it.data.children }
                .map { it.listIterator() }
                .map { mapper.transformList(it) }
    }
}