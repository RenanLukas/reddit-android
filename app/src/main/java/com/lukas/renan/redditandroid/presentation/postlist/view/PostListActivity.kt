package com.lukas.renan.redditandroid.presentation.postlist.view

import android.net.Uri
import android.os.Bundle
import android.support.customtabs.CustomTabsIntent
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.lukas.renan.redditandroid.R
import com.lukas.renan.redditandroid.data.base.remote.RedditAPIBuilder
import com.lukas.renan.redditandroid.data.postlist.mapper.RedditPostDataEntityMapper
import com.lukas.renan.redditandroid.data.postlist.repository.PostListDataRepository
import com.lukas.renan.redditandroid.domain.postlist.Post
import com.lukas.renan.redditandroid.domain.postlist.PostListInteractor
import com.lukas.renan.redditandroid.presentation.base.BaseActivity
import com.lukas.renan.redditandroid.presentation.mechanism.extension.hide
import com.lukas.renan.redditandroid.presentation.mechanism.extension.show
import com.lukas.renan.redditandroid.presentation.postlist.PostListContract
import com.lukas.renan.redditandroid.presentation.postlist.presenter.PostListPresenter
import jp.wasabeef.recyclerview.adapters.AlphaInAnimationAdapter
import kotlinx.android.synthetic.main.activity_postlist.*
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

class PostListActivity : BaseActivity<PostListPresenter>(), PostListContract.View {

    private var adapter: PostListAdapter? = null

    private var firstLoad: Boolean = true

    private var shouldLoadMore: Boolean = true

    override fun createPresenter(): PostListPresenter = PostListPresenter(this,
            PostListInteractor(PostListDataRepository(RedditAPIBuilder().build(), RedditPostDataEntityMapper.instance),
                    Schedulers.io(), AndroidSchedulers.mainThread()))

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_postlist)
        configurePostList()
        configureErrorLayout()
        presenter?.present()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter?.finish()
    }

    private fun configureErrorLayout() {
        retry.setOnClickListener({
            presenter?.present()
        })
    }

    private fun configurePostList() {
        adapter = PostListAdapter(resources)
        val animationAdapter = AlphaInAnimationAdapter(adapter)
        animationAdapter.setDuration(500)
        val layoutManager = LinearLayoutManager(this)
        postlist.layoutManager = layoutManager
        postlist.adapter = animationAdapter
        refresh_postlist.setOnRefreshListener {
            presenter?.onPostListPulled(adapter?.newestPostId())
        }
        postlist.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
                if (dy > 0) {
                    val visibleItemCount = layoutManager.childCount
                    val totalItemCount = layoutManager.itemCount
                    val pastVisibleItems = layoutManager.findFirstVisibleItemPosition()

                    if ((visibleItemCount + pastVisibleItems) >= totalItemCount && shouldLoadMore) {
                        firstLoad = false
                        shouldLoadMore = false
                        presenter?.onPostListScrolledToEnd(adapter?.oldestPostId())
                    }
                }
            }
        })
        adapter?.clickObservable()?.subscribe({
            presenter?.onPostClicked(it)
        })
    }

    override fun updatePostList(posts: List<Post>) {
        refresh_postlist.isRefreshing = false
        adapter?.update(posts)
        shouldLoadMore = true
    }

    override fun navigateToExternalWebScreen(url: String) {
        val customTabsBuilder = CustomTabsIntent.Builder()
        customTabsBuilder.setToolbarColor(getColor(R.color.colorPrimary))
        val customTabsIntent = customTabsBuilder.build()
        customTabsIntent.launchUrl(this, Uri.parse(url))
    }

    override fun showErrorLayout() {
        error_layout.show()
        progress.hide()
        refresh_postlist.hide()
    }

    override fun showLoadingLayout() {
        progress.show()
        error_layout.hide()
        refresh_postlist.hide()
    }

    override fun showListLayout() {
        refresh_postlist.show()
        error_layout.hide()
        progress.hide()
    }
}