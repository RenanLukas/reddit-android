package com.lukas.renan.redditandroid.data.postlist.entity

data class RedditPostListing(val kind: String, val data: RedditPostListingData)