package com.lukas.renan.redditandroid.data.postlist.mapper

import com.lukas.renan.redditandroid.data.postlist.entity.RedditPostData
import com.lukas.renan.redditandroid.domain.postlist.Post

open class RedditPostDataEntityMapper private constructor() {

    private val REDDIT_SELF_DOMAIN_PREFIX = "self."

    private object Holder {
        val INSTANCE = RedditPostDataEntityMapper()
    }

    companion object {
        val instance: RedditPostDataEntityMapper by lazy { Holder.INSTANCE }
    }

    fun transformList(iterator: ListIterator<RedditPostData>): List<Post> {
        val posts: MutableList<Post> = mutableListOf()
        while (iterator.hasNext()) {
            posts.add(transform(iterator.next()))
        }
        return posts
    }

    fun transform(redditPostData: RedditPostData): Post {
        val postEntity = redditPostData.data
        val thumbnailUrl = postEntity.media?.oembed?.thumbnail_url ?: postEntity.thumbnail
        return Post(postEntity.name, postEntity.title, postEntity.url, thumbnailUrl, postEntity.num_comments,
                postEntity.created, !postEntity.domain.startsWith(REDDIT_SELF_DOMAIN_PREFIX))
    }
}