package com.lukas.renan.redditandroid.data.postlist.entity

data class RedditPostMediaWrapper(val oembed: RedditPostMedia)