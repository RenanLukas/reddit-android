package com.lukas.renan.redditandroid.data.postlist.entity

data class RedditPostListingData(val modhash: String, val children: List<RedditPostData>)