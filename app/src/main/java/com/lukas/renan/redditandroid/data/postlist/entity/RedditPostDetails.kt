package com.lukas.renan.redditandroid.data.postlist.entity

data class RedditPostDetails(val name: String, val title: String, val url: String, val thumbnail: String, val num_comments: Int,
                             val created: Long, val domain: String, val media: RedditPostMediaWrapper?)