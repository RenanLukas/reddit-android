package com.lukas.renan.redditandroid.data.postlist.entity

data class RedditPostMedia(val thumbnail_url: String)