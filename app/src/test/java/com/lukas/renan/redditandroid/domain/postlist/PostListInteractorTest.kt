package com.lukas.renan.redditandroid.domain.postlist

import com.lukas.renan.redditandroid.data.postlist.repository.PostListDataRepository
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.*
import rx.Scheduler

class PostListInteractorTest {

    var interactor: PostListInteractor = mock()

    val FAKE_POST_ID = "FAKE_POST_ID"

    val repository: PostListDataRepository = mock()
    val subscribeScheduler: Scheduler = mock()
    val observeScheduler: Scheduler = mock()

    @Before
    fun setUp() {
        interactor = PostListInteractor(repository, subscribeScheduler, observeScheduler)
    }

    @Test
    fun testPostListInteractorObservableHappyCase() {
        interactor.buildObservable()

        verify(repository).fetchPostList(null, null)
        verifyNoMoreInteractions(repository)
        verifyZeroInteractions(subscribeScheduler)
        verifyZeroInteractions(observeScheduler)
    }

    @Test
    fun testPostListInteractorObservableFetchNewerHappyCase() {
        interactor.fetchNewerThan(FAKE_POST_ID)
        interactor.buildObservable()

        verify(repository).fetchPostList(FAKE_POST_ID, null)
        verifyNoMoreInteractions(repository)
        verifyZeroInteractions(subscribeScheduler)
        verifyZeroInteractions(observeScheduler)
    }

    @Test
    fun testPostListInteractorObservableFetchOlderHappyCase() {
        interactor.fetchOlderThan(FAKE_POST_ID)
        interactor.buildObservable()

        verify(repository).fetchPostList(null, FAKE_POST_ID)
        verifyNoMoreInteractions(repository)
        verifyZeroInteractions(subscribeScheduler)
        verifyZeroInteractions(observeScheduler)
    }
}

inline fun <reified T : Any> mock(): T = Mockito.mock(T::class.java)