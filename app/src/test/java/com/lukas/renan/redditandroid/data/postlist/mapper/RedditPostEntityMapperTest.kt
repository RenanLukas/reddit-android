package com.lukas.renan.redditandroid.data.postlist.mapper

import com.lukas.renan.redditandroid.data.postlist.entity.RedditPostData
import com.lukas.renan.redditandroid.data.postlist.entity.RedditPostDetails
import com.lukas.renan.redditandroid.data.postlist.entity.RedditPostMedia
import com.lukas.renan.redditandroid.data.postlist.entity.RedditPostMediaWrapper
import com.lukas.renan.redditandroid.domain.postlist.Post
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.instanceOf
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.runners.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class RedditPostEntityMapperTest {

    val FAKE_NAME = "Name"
    val FAKE_TITLE = "Title"
    val FAKE_URL = "www.reddit.com"
    val FAKE_THUMBNAIL = "thumbnail_url"
    val FAKE_NUMCOMMENTS = 123
    val FAKE_CREATED = 123L
    val FAKE_DOMAIN = "self."
    val FAKE_MEDIAWRAPPER = RedditPostMediaWrapper(RedditPostMedia("media_thumbnail_url"))

    var mapper: RedditPostDataEntityMapper = mock()

    @Before
    fun setUp() {
        mapper = RedditPostDataEntityMapper.instance
    }

    @Test
    fun testTransformRedditPostDataWithoutMediaEntity() {
        val redditPostDataEntity = RedditPostData("", RedditPostDetails(FAKE_NAME, FAKE_TITLE, FAKE_URL, FAKE_THUMBNAIL, FAKE_NUMCOMMENTS,
                FAKE_CREATED, FAKE_DOMAIN, null))
        val post = mapper.transform(redditPostDataEntity)

        assertThat(post, `is`(instanceOf(Post::class.java)))
        assertThat(post.name, `is`(FAKE_NAME))
        assertThat(post.title, `is`(FAKE_TITLE))
        assertThat(post.url, `is`(FAKE_URL))
        assertThat(post.num_comments, `is`(FAKE_NUMCOMMENTS))
        assertThat(post.created, `is`(FAKE_CREATED))
        assertThat(post.thumbnail, `is`(FAKE_THUMBNAIL))
    }

    @Test
    fun testTransformRedditPostDataWithMediaEntity() {
        val redditPostDataEntity = createFakeRedditPostData()
        val post = mapper.transform(redditPostDataEntity)

        assertThat(post, `is`(instanceOf(Post::class.java)))
        assertThat(post.name, `is`(FAKE_NAME))
        assertThat(post.title, `is`(FAKE_TITLE))
        assertThat(post.url, `is`(FAKE_URL))
        assertThat(post.num_comments, `is`(FAKE_NUMCOMMENTS))
        assertThat(post.created, `is`(FAKE_CREATED))
        assertThat(post.thumbnail, `is`(FAKE_MEDIAWRAPPER.oembed.thumbnail_url))
    }

    @Test
    fun testTransformRedditPostDataCollection() {
        val postDataEntityOne: RedditPostData = createFakeRedditPostData()
        val postDataEntityTwo: RedditPostData = createFakeRedditPostData()
        val postDataEntities = listOf(postDataEntityOne, postDataEntityTwo)

        val postCollection = mapper.transformList(postDataEntities.listIterator())

        assertThat(postCollection[0], `is`(instanceOf(Post::class.java)))
        assertThat(postCollection[1], `is`(instanceOf(Post::class.java)))
        assertThat(postCollection.size, `is`(2))
    }

    private fun createFakeRedditPostData(): RedditPostData {
        return RedditPostData("", RedditPostDetails(FAKE_NAME, FAKE_TITLE, FAKE_URL, FAKE_THUMBNAIL, FAKE_NUMCOMMENTS,
                FAKE_CREATED, FAKE_DOMAIN, FAKE_MEDIAWRAPPER))
    }
}

inline fun <reified T : Any> mock(): T = Mockito.mock(T::class.java)