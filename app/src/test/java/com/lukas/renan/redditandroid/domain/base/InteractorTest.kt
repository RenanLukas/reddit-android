package com.lukas.renan.redditandroid.domain.base

import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.Is.`is`
import org.junit.Test
import org.mockito.Mockito
import rx.Observable
import rx.Scheduler
import rx.observers.TestSubscriber

class InteractorTest {

    val subscribeScheduler: Scheduler = mock()
    val observeScheduler: Scheduler = mock()

    @Test
    @SuppressWarnings("unchecked")
    fun testBuildUseCaseObservableReturnCorrectResult() {
        val interactor = InteractorTestClass<Int>(subscribeScheduler, observeScheduler)
        val testSubscriber = TestSubscriber<Int>()
        interactor.execute(testSubscriber)

        assertThat(testSubscriber.onNextEvents.size, `is`(0))
    }

    @Test
    fun testSubscriptionWhenExecutingUseCase() {
        val interactor = InteractorTestClass<Int>(subscribeScheduler, observeScheduler)
        val testSubscriber = TestSubscriber<Int>()

        interactor.execute(testSubscriber)
        interactor.unsubscribe()

        assertThat(testSubscriber.isUnsubscribed, `is`(true))
    }

    open class InteractorTestClass<T : Any>(subscribeScheduler: Scheduler, observerScheduler: Scheduler) :
            Interactor<T>(subscribeScheduler, observerScheduler) {
        override fun buildObservable(): Observable<T> {
            return Observable.empty()
        }
    }
}

inline fun <reified T : Any> mock(): T = Mockito.mock(T::class.java)