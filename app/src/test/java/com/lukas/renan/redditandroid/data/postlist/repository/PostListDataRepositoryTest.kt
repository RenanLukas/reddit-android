package com.lukas.renan.redditandroid.data.postlist.repository

import com.lukas.renan.redditandroid.data.base.remote.RedditAPI
import com.lukas.renan.redditandroid.data.postlist.entity.*
import com.lukas.renan.redditandroid.data.postlist.mapper.RedditPostDataEntityMapper
import com.lukas.renan.redditandroid.domain.postlist.Post
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.Matchers
import org.mockito.Mockito
import org.mockito.runners.MockitoJUnitRunner
import rx.Observable

@RunWith(MockitoJUnitRunner::class)
class PostListDataRepositoryTest {

    var repository: PostListDataRepository = mock()
    var api: RedditAPI = mock()
    var mapper: RedditPostDataEntityMapper = mock()
    var apiObservable: Observable<RedditPostListing> = mock()
    var domainObservable: Observable<List<Post>> = mock()

    @Before
    fun setUp() {
        api = mock()
        mapper = mock()
        repository = PostListDataRepository(api, mapper)

        given(api.fetchNewAndroidPosts(10, "", "")).willReturn(apiObservable)
        given(repository.fetchPostList("", "")).willReturn(domainObservable)
    }

    @Test
    fun testFetchPostsHappyCase() {
        val redditPostDataList: List<RedditPostData> = listOf(RedditPostData("",
                RedditPostDetails("", "", "", "", 123, 123, "", RedditPostMediaWrapper(RedditPostMedia("")))))
        val redditPostListing: RedditPostListing = RedditPostListing("", RedditPostListingData("", redditPostDataList))
        given(api.fetchNewAndroidPosts(Matchers.anyInt(), Matchers.anyString(), Matchers.anyString())).willReturn(Observable.just(redditPostListing))

        repository.fetchPostList("", "")

        Mockito.verify(api).fetchNewAndroidPosts(Matchers.anyInt(), Matchers.anyString(), Matchers.anyString())
    }
}

inline fun <reified T : Any> mock(): T = Mockito.mock(T::class.java)