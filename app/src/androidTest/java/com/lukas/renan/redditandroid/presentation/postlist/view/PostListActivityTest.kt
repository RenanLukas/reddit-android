package com.lukas.renan.redditandroid.presentation.postlist.view

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.assertion.ViewAssertions
import android.support.test.espresso.intent.rule.IntentsTestRule
import android.support.test.espresso.matcher.ViewMatchers.isDisplayed
import android.support.test.espresso.matcher.ViewMatchers.withId
import android.support.test.runner.AndroidJUnit4
import com.lukas.renan.redditandroid.R
import org.hamcrest.Matchers.not
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class PostListActivityTest {

    @Rule @JvmField
    var activityRule: IntentsTestRule<PostListActivity> = IntentsTestRule(PostListActivity::class.java)

    @Test
    fun testViewsShownHappyCase() {
        onView(withId(R.id.error_layout)).check(ViewAssertions.matches(not(isDisplayed())))
        onView(withId(R.id.retry)).check(ViewAssertions.matches(not(isDisplayed())))

        onView(withId(R.id.refresh_postlist)).check(ViewAssertions.matches(isDisplayed()))
        onView(withId(R.id.postlist)).check(ViewAssertions.matches(isDisplayed()))
    }
}